export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'dashboard',
      badge: {
        variant: 'primary',
        text: 'NEW'
      }
    },
    {
      name: 'Translations',
      url: '/translations/list',
      icon: 'translate'
    }
  ]
}
