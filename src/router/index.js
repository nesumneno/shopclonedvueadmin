import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '@/containers/Full'

// Views
import Dashboard from '@/views/Dashboard'

import ProductsList from '@/views/products/ProductsList'
import TranslationsList from '@/views/translations/TranslationsList'

// Views - Pages
import Page404 from '@/views/pages/Page404'
import Page500 from '@/views/pages/Page500'

Vue.use(Router)

const router = new Router({
  mode: 'hash', // Demo is living in GitHub.io, so required!
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/:lang',
      name: 'Home',
      component: Full,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'translations',
          redirect: '/:lang/translations/list',
          name: 'Translations',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'list',
              name: 'Translations List',
              component: TranslationsList
            },
            {
              path: 'products',
              name: 'Products List',
              component: ProductsList
            }
          ]
        },
        {
          path: '/:lang/pages',
          redirect: '/:lang/pages/404',
          name: 'Pages',
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '404',
              name: 'Page404',
              component: Page404
            },
            {
              path: '500',
              name: 'Page500',
              component: Page500
            }
          ]
        }
      ]
    }
  ]
})

router.beforeEach(function (to, from, next) {
  var path
  if (to.params.lang && to.params.lang.length === 2) {
    Vue.prototype.$trans.lang = to.params.lang
    if (to.name === 'Home') {
      next(to.path + '/' + 'dashboard')
    } else {
      next()
    }
  } else {
    (to.path === '/' ? path = '' : path = to.path)
    next('/' + Vue.prototype.$trans.lang + path)
  }
})

export default router
