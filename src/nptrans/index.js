import Vue from 'vue'
import Config from '@/config'

const NpTrans = {
  install () {
    Vue.prototype.$trans = new Vue({
      localStorage: {
        lang: {
          type: String,
          default: Config.defaultLang
        },
        translations: {
          type: Object,
          default: {}
        },
        pendingTranslations: {
          type: Array,
          default: []
        }
      },
      data () {
        return {
          lang: localStorage.getItem('lang', Config.defaultLang)
        }
      },
      watch: {
        lang: function (value) {
          this.lang = value
          this.$localStorage.lang = value
        }
      }
    })
  }
}

export default NpTrans
