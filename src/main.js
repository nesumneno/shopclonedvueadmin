// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import store from './store'
import router from './router'

import npTrans from './nptrans'

import Vuetify from 'vuetify'
import VueLocalStorage from 'vue-localstorage'
import LodashForVue from 'lodash-for-vue'

Vue.use(LodashForVue)
Vue.use(VueLocalStorage)
Vue.use(npTrans)

Vue.use(Vuetify, {
  theme: {
    primary: '#263238',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
})

require('./mixins/globalTransMixin')
/* eslint-disable no-new */

Vue.prototype.$style = new Vue({
  localStorage: {
    darkTheme: {
      type: Boolean,
      default: true
    },
    showDrawer: {
      type: Boolean,
      default: true
    },
    theme: {
      type: Object,
      default: {
        primary: '#263238',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107'
      }
    }
  }
})

new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  beforeMount: function () {
    this.$store.dispatch('getAllTranslations', this.$trans.lang)
  },
  components: {
    App
  }
})
