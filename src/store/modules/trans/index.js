import trans from '@/api/trans'

// initial state
const state = {
  all: {}
}

// getters
const getters = {
  allTranslations: state => state.all
}

// actions
const actions = {
  getAllTranslations ({ commit }, lang) {
    trans.getCachedTranslations(lang, 'backend', translations => {
      commit('setTranslations', translations)
    })
  }
}

// mutations
const mutations = {
  setTranslations (state, translations) {
    state.all = translations
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
