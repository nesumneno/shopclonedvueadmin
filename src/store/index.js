import Vue from 'vue'
import Vuex from 'vuex'

import trans from './modules/trans'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    trans
  }
})
