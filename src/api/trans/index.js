import Config from '@/config'
import Request from 'request'

// Getting all cached translations from Laravel and storing them to Local Storage
const Api = {
  getCachedTranslations: function (lang, group, cb) {
    var transObject = {
      bg: {},
      en: {}
    }
    var url = Config.apiUrl + 'manage/translations/get-cached-translations?lang=' + lang + '&group=' + group
    Request(url, function (error, response, body) {
      if (response && response.statusCode === 200) {
        var resultTranslations = JSON.parse(body)

        transObject[lang][group] = resultTranslations
        localStorage.setItem('translations', transObject)
        cb(transObject[lang])
      } else {
        console.log('error:', error)
        console.log('statusCode:', response && response.statusCode)
      }
    })
  }
}

export default Api
