import Config from '@/config'
import Request from 'request'

export default {
  data () {
    return {
      snackbar: {
        show: false,
        color: 'success',
        text: 'Success'
      },
      loading: true,
      pagination: {},
      trueOrderType: 'asc',
      items: [],
      confirmDelete: false,
      apiUrl: Config.apiUrl,
      editedIndex: -1,
      headers: [],
      rowsPerPage: [10, 25, 50, {'text': 'All', 'value': -1}]
    }
  },
  computed: {
    page: function () {
      return this.pagination.page
    },
    perPage: function () {
      return this.pagination.rowsPerPage
    },
    orderBy: function () {
      return this.pagination.sortBy ? this.pagination.sortBy : 'id'
    },
    orderType: function () {
      return this.pagination.descending ? this.pagination.descending : false
    }
  },
  watch: {
    filters: {
      handler: function (value) {
        this.pagination.page = 1
        this.fetchData(this.page, value, this.perPage, this.orderBy, this.trueOrderType)
      },
      deep: true
    },
    page: function (value) {
      this.fetchData(value, this.filters, this.perPage, this.orderBy, this.trueOrderType)
    },
    orderBy: function (value) {
      if (this.loading === false) {
        if (this.orderType === false || this.orderType === null) {
          this.trueOrderType = 'asc'
        } else {
          this.trueOrderType = 'desc'
        }
        this.pagination.page = 1
        this.fetchData(this.page, this.filters, this.perPage, value)
      }
    },
    orderType: function (value) {
      if (this.loading === false) {
        if (value === false || value === null) {
          this.trueOrderType = 'asc'
        } else {
          this.trueOrderType = 'desc'
        }
        this.pagination.page = 1
        this.fetchData(this.page, this.filters, this.perPage, this.orderBy, this.trueOrderType)
      }
    }
  },
  methods: {
    deleteConfirmation: function (index) {
      this.editedIndex = index
      this.confirmDelete = true
    },
    deleteItem: function () {
      let self = this
      let item = self.items.data[this.editedIndex]
      const url = Config.apiUrl + this.deleteUrl

      Request.post({url: url, form: item}, function optionalCallback (err, httpResponse, body) {
        self.snackbar = {
          show: true,
          color: 'success',
          text: 'Success'
        }
        self.items.data.splice(self.editedIndex, 1)
        self.confirmDelete = false
        self.editedIndex = -1

        if (err) {
          console.log(err)
          console.log(httpResponse)
        }
      })
    }
  }
}
