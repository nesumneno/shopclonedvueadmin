import Vue from 'vue'

Vue.mixin({
  methods: {
    __: function (key, group = 'backend', type = 0) {
      var translations = this.$store.getters.allTranslations
      const match = Vue.$lodash.findIndex(this.$localStorage.pendingTranslations, function (o) {
        return o.lang_key === key
      })
      if (translations[group] && translations[group][key]) {
        if (match > -1) {
          this.removePendingTrans(match)
        }
        return translations[group][key]
      } else {
        if (match === -1) {
          this.addPendingTrans(key, group, type)
        }
        return key
      }
    },
    addPendingTrans: function (key, group, type) {
      const pendingTranslations = this.$localStorage.pendingTranslations
      const newId = pendingTranslations[0] ? (pendingTranslations[0].id - 1) : -1
      pendingTranslations.unshift(
        {
          id: newId,
          lang_key: key,
          lang_group: group,
          input_type: type,
          text: {
            bg: key,
            en: key
          }
        }
      )
      this.$localStorage.pendingTranslations = pendingTranslations
    },
    removePendingTrans: function (index) {
      const pendingTranslations = this.$localStorage.pendingTranslations
      pendingTranslations.splice(index, 1)
      this.$localStorage.pendingTranslations = pendingTranslations
    },
    translateArrayObjects: function (items, field) {
      console.log('in')
      let self = this
      if (items) {
        items.forEach(function (value, index) {
          items[index].text = self.__(value[field])
        })
      }

      return items
    }
  }
})
